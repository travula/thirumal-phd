#+TITLE: Index to Learning Plan
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-05-15 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
The learning plan to to build the skills required for my
pursuit of phd.

* Mathematics
Calculus, Probability, Statistical Inferance, Linear
Algebra, Optimization etc are some of the borad topics that
are needed to understand AI and apply machine learning.
** Resources
1. I enrolled for the courses precalculus - functions and
   trigonometry on calculus on [[coursera]].
2. Single Variable Calculus - part 1, 2, 3, 4, 5 on coursera
   or the [[https://ocw.mit.edu/courses/mathematics/18-01-single-variable-calculus-fall-2006/index.htm][MIT course]]
3. Multi Variable Calculus, [[https://ocw.mit.edu/courses/mathematics/18-02-multivariable-calculus-fall-2007/index.htm][MIT]]
4. [[https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/][Linear Algebra]]
5. Introduction to Probability, [[https://courses.edx.org/courses/course-v1:MITx+6.041x_4+1T2017/course/][edx]]

* Programming Languages
All computations are made through a computer.  The interface
between the human and computer is the programming language.
Though it is important to know the syntax of a programming
to talk to the computer, it is also important that we
understand the semantics of a programming languages.  The
semantics are applied by the interpreter while evaluation of
the expressions.  Study of semantics will lead us to define
our own semantics for user defined evaluation, though this
might be rare, the understanding gives us an insight into
the structures of how languages are built and allows us to
choose the right language for a set of problems and harness
the language to the hilt. 


There are different paradigms of programming languages -
functional, OO, imperative and logical. 

To facilitate such an path, I choose these learning
resources:
1. Programming Languages Parts A, B, C on coursera by Don Grossman
2. Lectures on SICP
3. Read the book SICP
4. Read the book EOPL
5. Enroll for the course POPL.



* Algorithms
  Complete this program with 4 courses on [[https://www.coursera.org/specializations/algorithms][coursera]]
  


