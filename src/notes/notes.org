#+TITLE: Notes on Ideas
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-04-09 Tue]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* [2019-04-09 Tue]
The first task is to determine how ICT can facilitate
setting up a path that orients the function of an
cooperative to effectively collaborate in a multi agent
system.

Once we determine that ICT has a role in such facilitation,
the next task would be to identify and build the primitives
that are used to compose these individual systems.  If we
define ontology as a /specification of conceptualization/
(Tom Gruber), then what follows is a formalization of these
systems that are oriented to perform a certain function.
Once such a formalization exists, then the door opens for
automating building of such systems.


* [2019-04-10 Wed]

  - competing with, rather than exchanging with each other [[http://delivery.acm.org/10.1145/2990000/2984074/p12-crenshaw.pdf?ip=14.139.82.6&id=2984074&acc=ACTIVE%2520SERVICE&key=045416EF4DDA69D9%252E1E2B3508530718A8%252E4D4702B0C3E38B35%252E4D4702B0C3E38B35&__acm__=1554880677_1f0dbdf5bd5c07418f8a3a33907fa34a][ref]]
  - 





