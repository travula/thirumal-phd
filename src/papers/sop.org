#+TITLE: Statement of Purpose
#+AUTHOR: Thirumal Ravula
#+DATE: [2017-10-20 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


What has dawned upon me is that time does not wait.  I
completed 4 years this month at Virtual Labs and right now
it does not seem that long.  The primary reason for moving
to Virtual Labs from industry and after earning a masters in
Computer Science and working in industry is to figure out
what is the best way to sell my labor.  Some of the
questions I have been asking all this while from my high
school is the purpose of educating myself but the tide took
me along and I consider myself lucky to get the
opportunities to study, work and therefore to spend and lead
a decent life.  I learned the tricks and the skills that
were appropriate for the job market but the question
remained: What am I working for?

This question allowed me to come back to India and later
allowed me to quit my corporate job and work at Virtual
Labs.

Capital grew it's dominance when the symbiotic relation
between the group composed of artisans who possessed skills
to manufacture goods; and the other group that went and sold
these goods; morphed into labor and capital classes where
one group exploited the other.  When capital accumulated and
new avenues to be prospected grew, the decision to impart
necessary new skills moved into domain of the capital.  I
gained this understanding from reading the yet to be
completed book -

IT in India is in the clutches of a few people and when a
growing demand for the artisans manifested, it is all chaos.
The industry did not emanate from these artisans, instead to
accomplish some other tasks, primarily in activities that
perpetuate the furtherance of the hegemony of the capital,
the Indian IT industry sprouted up.  The problem with the
growth of anything in the most inorganic way is a dichotomy
between human labor and dignity in the real sense where
labor's primary function is to sow the seeds of originality.

I believe decentralization is essential and smaller
sustainable communities are the solution for many problems
that afflict the mankind.  Trust and collaboration will be
the ingredients of the glue that will not only hold a
particular community together but can ideally bring many
such communities to work together for each individual's
benefit.

Today, platforms like github or gitlab allow collaborative
software system building.  There are also platforms that
allow collaborative content writing like wikipedia.  What
fascinates me is the thought of extending the collaboration
to other fields like governance, education, commerce, etc.

Governance has two facets to it.  The act of making
decisions on how to govern and the act of implementing these
decisions.  Can a system help play each scene as the
governance is getting enacted or is unfolding?  The
smartness in governance should be a direct function of
citizen participation in governance.  How should a platform
be structured to facilitate policy makers, bureaucracy and
the citizens collaborate in effecting participatory
governance?

In education, the platform that allows authoring and
delivery of virtual labs ought to be collaborative.  An
experiment to get to the stage of publishing would require
multiple hand-offs between various actors - say a pedagogy
expert, an instructional designer, a graphic designer, etc.
A tight feedback from the students should help evolve the
content.

In commerce too, many redundancies could be done away in a
platform that allows collaboration between the actual
producers and the consumers.  What does it take for a
producers' co-operative(s) to directly engage with
consumers' co-operative(s)? - a system that has the ability
to trace each transaction from the raw material to the
delivery of the finished product.

Are there any underlying similarities between the above
platforms that allow collaboration?  Is there a basic
principle or a set of principles that can be modelled for
all such platforms?  The state transformation of a system
can be modelled as a function that reacts to the input to
move to another state while maintaining a set of
in-variants.  Then any collaborative platform which by
itself is a state machine can be envisaged as a composition
of such systems.  The challenge is to define such an
abstraction to quickly build and compose platforms.

I fathom that one of the ways to glean this abstraction is
to build multiple such platforms.  The motivation for me is
two fold here: this process of research and building systems
will ground me soundly in computer science; and the other is
to participate and influence the policy to adapt such
platforms that help transparency, collaboration and trust.
