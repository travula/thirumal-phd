#+TITLE: Work Logs for February 2020
#+AUTHOR: Thirumal Ravula
#+DATE: [2020-01-02 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
Work logs for the month of February.  [[http://www.whatweekisit.org/calendar-2020.html][Reference]] for week
numbers. 

* [2020-02-25 Tue]
These are notes from the meeting with Venkatesh and other
research students - Ojas, and Raj
  1. Paper - Cluster
  2. Pick a problem, the methods change
  3. Evolution of software processes and evolution of living
     systems
  4. Paper
     1. do the necessary literature survey
     2. citations
     3. references
     4. 
  5. have 3-4 talks ready, always have a slide deck
  6. SCIP, ICSE conferences
  7. Gather the ontology of all other components

