#+TITLE: Index to the progress
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-11-25 Mon
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Introduction
This is index to the progress on the research. 

* [[./analogy-with-software.org][An analogy to software]]
