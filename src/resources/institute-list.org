#+TITLE: List of Institutes
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-08-16 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document contains the list of institutes that work in
  the same areas of interest.

* Research and Policy Institutes
  1. https://neweconomics.org/
  2. https://www.epi.org/
  3. https://thenextsystem.org/about-next-system-project
  4. https://democracycollaborative.org/

* Coop Organizations
  1. https://institute.coop/ - Democracy at Work
  2. [[https://community-wealth.org/][Community Wealth]]
  3. https://coopseurope.coop

* Funding Agencies
  1. [[https://rosalux.in/][Rosa Luxemberg Stiftung, South Aisa]]
  2. [[https://in.boell.org/][Heinrich Boll Stiftung, India]]
