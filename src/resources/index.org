#+TITLE: List of Resoruces
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-04-17 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Zotero
[[https://www.zotero.org/][Zotero]] allows collection and sharing of bibliography among
collaborators.  [[https://www.zotero.org/groups/2340951/cooperatives][cooperatives]] is the group where the current
bibliography is collected.

** Installation
Follow these [[https://askubuntu.com/a/332117][instructions]].

** Idiosyncrasies
   - [[https://askubuntu.com/questions/941833/failed-to-update-zotero-after-changing-permission-with-chmod-707][Failed to update Zotero]]

** Firefox plugin
This [[https://www.zotero.org/download/connectors][plugin]] allows saving to zotero from the browsing
window.

* Institutes
  [[./institute-list.org][List]] of institutes whose research or activism intersects
  with our problem.

* CS Resources
  [[./cs-resources.org][List]] of interest groups and conferences that deal with
  same ideas as our current research.

* COMMENT List
1. [[https://democracycollaborative.org/content/next-system-project][Democracy Collaborative's The Next System Project]]
2. [[https://community-wealth.org/content/cleveland-model-how-evergreen-cooperatives-are-building-community-wealth][Evergreen Cooperatives]]
3. [[https://www.iisd.org/pdf/2010/icts_internet_sd_new_paradigm.pdf][ICT for sustainable development]]
4. [[http://www.eolss.net/Sample-chapters/C02/E6-46-01-05.pdf][Actor Systems Dynamic Theory]]
5. [[https://arxiv.org/pdf/1711.08319.pdf][Extending Actors to General Systems Theory]]
6. [[http://www.nzdl.org/gsdlmod?e=d-00000-00---off-0cdl--00-0----0-10-0---0---0direct-10---4-------0-1l--11-en-50---20-about---00-0-1-00-0--4----0-0-11-10-0utfZz-8-00&cl=CL1.61&d=HASH01e58d563aadbe4e1f5faafb.13&gt=1][A systems Approach to Cooperatives]]
7. [[https://business.leeds.ac.uk/research-and-innovation/research-centres/stc/socio-technical-systems-theory/][Socio Technical Theory]] :: Socio-technical theory has at
     its core the idea that the design and performance of
     any organisational system can only be understood and
     improved if both 'social' and 'technical' aspects are
     brought together and treated as interdependent parts of
     a complex system.
8. [[https://coopseurope.coop/sites/default/files/Paper_Cooperatives%2520Collab%2520Economy__0.pdf][Cooperatives and Collaboration]] :: Overall, the biggest
     challenge is to apply cooperative values online, and to
     operationalise cooperative principles (as defined by
     the International Cooperative Alliance) through new
     forms of cooperation that may be quite different from
     those used for the past couple of centuries.


Interested in applying Systems Theory and Actor Model of
Computation to allow functioning of Cooperatives for a
sustainable economic model.

Can we tie the actor model in computer science to the system
theory and the actor systems dynamic theory to realize
working systems that exhibit certain characteristics? for
example, can the actors in computational systems embody the
properties and dynamics modeled by the systems and ASD
theories to build a cooperative economic model as an
alternative to extractive capitalist model?

The inability to imagine a different life is capital's
ultimate triumph.  -  Platform cooperativism vs the sharing
economy, Scholz, T


Problem of Network effect, Entry Barrier - federated c

