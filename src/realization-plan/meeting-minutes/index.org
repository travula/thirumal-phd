#+TITLE: Index to Meeting Minutes
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-08-16 Fri]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This is index to the meetings.

* Meeting List
** [[./2019-08-21.org][2019-08-21]]
** [[./2019-08-01.org][2019-08-01]]
** [[./2019-06-15.org][2019-06-15]]
** [[./2019-05-13.org][2019-05-13]]
** [[./2019-05-06-problem-stmt.org][2019-05-06]]
** [[./2018-04-05.org][2018-04-05]]




